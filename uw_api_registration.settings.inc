<?php

/**
 * @file
 * Module configuration page.
 */

/**
 * Generates the module configuration form.
 */
function uw_api_registration_settings() {
  $form = array();

  $form['uw_api_registration_registration_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Registration URL'),
    '#description' => t('URL to POST user registration details'),
    '#default_value' => variable_get('uw_api_registration_registration_url', 'https://api.uwaterloo.ca/keygen.php'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
