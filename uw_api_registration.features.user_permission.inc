<?php

/**
 * @file
 * uw_api_registration.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_api_registration_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer uw_api_registration'.
  $permissions['administer uw_api_registration'] = array(
    'name' => 'administer uw_api_registration',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'uw_api_registration',
  );

  return $permissions;
}
