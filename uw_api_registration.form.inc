<?php

/**
 * @file
 * The API registration form, which allows users to register with the University of Waterloo Open Data API.
 */

/**
 * Returns the UW API registration form.
 */
function uw_api_registration_form() {
  $form = array();

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Full name'),
    '#required' => TRUE,
  );

  $form['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#description' => t('Your API key will be emailed to this address.'),
    '#required' => TRUE,
  );

  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Register'),
  );

  return $form;
}

/**
 * Performs validation on the registration form.
 */
function uw_api_registration_form_validate(&$form, &$form_state) {
  if (!valid_email_address($form_state['values']['email'])) {
    form_set_error('email', t('Please provide a valid email address'));
  }
}

/**
 * Handles the submission of the UW API registration form.
 *
 * Submits the user's information to the API, and presents the result.
 */
function uw_api_registration_form_submit(&$form, &$form_state) {
  $api_registration_url = variable_get('uw_api_registration_registration_url', 'https://api.uwaterloo.ca/keygen.php');

  $headers = array('Content-Type' => 'application/x-www-form-urlencoded');
  $data = array(
    'n' => $form_state['values']['name'],
    'e' => $form_state['values']['email'],
  );

  $response = drupal_http_request($api_registration_url, array(
    'headers' => $headers,
    'method' => 'POST',
    'data' => drupal_http_build_query($data),
  ));

  if ($response->code != 200) {
    form_set_error('', t('Sorry, an unknown error occurred. Please try again at a later time.'));
  }
  else {
    $result = json_decode($response->data, TRUE);

    if ($result === NULL) {
      form_set_error('', t('Sorry, an unknown error occurred. Please try again at a later time.'));
    }
    elseif ($result['success']) {
      drupal_set_message(t($result['message']));
    }
    else {
      form_set_error('', t($result['error']));
    }
  }
}
